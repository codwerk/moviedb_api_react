import React from 'react';
import Nav from './Nav';
import Search from './Search';

class App extends React.Component {
  constructor() {
    super() 
    this.state = {
      movie: [],
      searchTerm:''
    }
    this.apiKey = process.env.REACT_APP_API
  }

  handleSubmit = (e)=> {
    e.preventDefault();

    fetch(`https://api.themoviedb.org/3/search/movie?api_key=${this.apiKey}&query=${this.state.searchTerm}`)
    .then(data => data.json())
    .then(data => {
      console.log(data);
      this.setState({ movie: [...data.results]})
    })
  }

  handleChange = (e)=> {
    this.setState({ searchTerm: e.target.value })
  }

  render() {
    return (
      <div className="App">
        <Nav />
        <Search handleSubmit={this.handleSubmit}  handleChange={this.handleChange}/>
      </div>
    );
  }
}

export default App;
