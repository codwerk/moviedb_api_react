import React from 'react';

class Nav extends React.Component {
  render() {
  return (
    <nav>
      <div className="nav-wrapper container">
          <a href="#" className="brand-logo">MovWi</a>
      </div>
    </nav>
  )
}
}

export default Nav;